	</div> <!-- /.container -->
  <footer class="siteFooter">
    <div class="container">
  		<div class="stylelist">
        <a href="http://www.stylelist.com/" target="_blank">
    			<img src="<?php bloginfo('template_url'); ?>/assets/img/stylelist.png" alt="Stylelist Contributor network">
        </a>
  		</div> <!-- /.stylelist -->

  		<div class="copyright">
  			<p>All Rights Reserved &copy; 9to5Chic, LLC 2013-<?php the_time('Y'); ?><br>
  						<a href="/privacy-policy">Privacy Policy</a>
  			</p>
  		</div>
    </div>
	</footer>



<!--- Google Analytics -->
  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-11688556-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>
<!-- End Google Analytics -->

<!--- Reward Style Tracking -->
  <script type="text/javascript">
    var _rsan = {};
    _rsan.key = '8ba6697d18069d85f549fe9dd2e8acb4b2ce5ae6';
    (function() {
      var rs = document.createElement('script');
      rs.type = 'text/javascript';
      rs.async = true;
      rs.src = ('https:' === document.location.protocol 
      ? 'https://collect' 
      : 'http://collect') 
      + '.rewardstyle.com/c.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(rs, s);
    })();
  </script>
<!--- End Reward Style Tracking -->
<!--WP Generated Footer -->
<?php wp_footer(); ?>
<!--End WP Generated Footer -->
</body>
</html>