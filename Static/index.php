<?php include("parts/header-home.php"); ?>

	<section class="upperArea">
		<div class="rotator flexslider">
			<ul class="slides">
				<li class="slideItem">
					<img src="http://placehold.it/1000x350" alt="">
					<div class="overlay">
						<div class="overlayContent">
							<h2>Suit</h2>
							<p>Taking my (mismatched) suit to a more casual place today with a casual striped top.</p>
							<a href="#" class="styledSubmit">Read More</a>
						</div> <!-- /.overlayContent -->
					</div> <!-- /.overlay -->
				</li>

				<li>
					<img src="http://placehold.it/1000x350" alt="">
					<div class="overlay">
						<div class="overlayContent">
							<h2>Slide 2</h2>
							<p>Taking my (mismatched) suit to a more casual place today with a casual striped top.</p>
							<a href="#" class="styledSubmit">Read More</a>
						</div> <!-- /.overlayContent -->
					</div> <!-- /.overlay -->
				</li>
			</ul>
		</div> <!-- /.rotator -->

		<section class="searchSocial">
			<div class="followEmail col-1-3">
				<input class="styledInput" type="text" placeholder="Follow Via Email">
				<input class="styledSubmit" type="submit" value="Submit">
			</div> <!-- /.followEmail -->

			<div class="socialIcons">
				<a href="" class="icon-plus-sign lovin"></a>
				<a href="" class="icon-twitter twitter"></a>
				<a href="" class="icon-facebook facebook"></a>
				<a href="" class="icon-pinterest pinterest"></a>
				<a href="" class="polyvore">P</a>
				<span class="currentSelection">Connect</span>
			</div> <!-- /.socialIcons -->

			</section>

			<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
			</div> <!-- /.headerAd -->
	</section>

	<div class="mainContent grid">

		<section class="posts col-2-3">
			<article class="blogPost">
				<p class="date"><time data-time="look into this">Monday, January 13, 2014</time></p>
				<h1 class="postTitle h2">Ut Enim</h1>
				<div class="postContent">
					<p>Eaque ipsa quae ab illo inventore veritatis et quasi. Nihil molestiae consequatur, vel illum qui dolorem eum. Duis aute irure dolor in reprehenderit in voluptate velit. Et harum quidem rerum facilis est et expedita distinctio. Animi, id est laborum et dolorum fuga.</p>
					<img src="http://placehold.it/590x400" alt="">
					<h2>Excepteur Sint</h2>
					<p>Et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque. Ut enim ad minim veniam, quis nostrud exercitation ullamco. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. Excepteur sint occaecat cupidatat non proident, sunt in culpa.</p>
					<ul>
					<li>Itaque earum rerum hic tenetur a sapiente delectus.</li>
					<li>Esse cillum dolore eu fugiat nulla pariatur.</li>
					<li>Nihil molestiae consequatur, vel illum qui dolorem eum.</li>
					</ul>
					<h3>Nam Libero Tempore</h3>
					<img src="http://placehold.it/590x400" alt="">
					<p>Cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia. Accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Do eiusmod tempor incididunt ut labore et dolore magna aliqua. Accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo.</p>
					<h4>Itaque Earum Rerum</h4>
					<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem. At vero eos et accusamus. Esse cillum dolore eu fugiat nulla pariatur. Et harum quidem rerum facilis est et expedita distinctio.</p>
					<ol>
					<li>Et harum quidem rerum facilis est et expedita distinctio.</li>
					<li>Laboris nisi ut aliquip ex ea commodo consequat.</li>
					<li>Do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
					<li>Excepteur sint occaecat cupidatat non proident, sunt in culpa.</li>
					</ol>
					<h5>Ipsam Vuptatem</h5>
					<p>Esse cillum dolore eu fugiat nulla pariatur. Nihil molestiae consequatur, vel illum qui dolorem eum. Qui officia deserunt mollit anim id est laborum.</p>
					<div class="shareButton"></div>
				</div> <!-- /.postContent -->
			</article>
		</section> <!-- /.posts -->

		<?php include("parts/sidebar.php"); ?>
	</div> <!-- /.mainContent -->

<?php include("parts/footer.php"); ?>