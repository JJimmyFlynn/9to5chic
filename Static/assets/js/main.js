//Variable for Instagram Ajax Call
// Swap out for 9to5Chic App client ID
var dataUrl = 'https://api.instagram.com/v1/users/4507990/media/recent';
var accessParameters = 'client_id=6ffbc1dd88ac46b5bd027f2c51f09577&count=10';

//Call for Latest Instagram Photos
$.ajax(dataUrl, {
	data: accessParameters,
	dataType: 'JSONP',
	//Append each image to flexslider
	success: function(data) {
		for (var i = data.data.length - 1; i >= 0; i--) {
			$('.instaSlides ul').append("<li>" + "<img src='" +
				data.data[i].images.standard_resolution["url"] +
				"'>");
		}
	}
	//Initialize Instagram flexslider when Ajax and Success calls done
}).done(function() {
	$('.instaSlides').flexslider({
		controlNav: false,
		directionNav: false,
	});
});

// Initialize Main Rotator Flexslider

$('.flexslider').flexslider({
	pauseOnHover: true,
	controlNav: false,
	directionNav: true,
	slideshowSpeed: 5000
});

//Initialize headroom.js
var headerOffset = $('.siteHeader').height() + $('.upperArea').height();
var elem = document.querySelector("header");
// construct an instance of Headroom, passing the element
var headroom  = new Headroom(elem, {
			"tolerance": 5,
			"classes": {
				"initial": "animated",
				"pinned": "slideInDown",
				"unpinned": "slideOutUp"
  }
});

if ($(window).width() > 780) {
	// initialise with waypoints @ .headerAd
	$('.headerAd').waypoint(function(direction) {
				headroom.init();
	});

	//fix header @ .mainContent to prevent flash of header
	$('.mainContent').waypoint(function(direction) {
			if(direction == "down") {
			$('.homeHeader').addClass('fixedHeader');
			$('.headerArea').addClass('isFixed');
		}
	});

	//destroy headroom when scrolling up @ .searchSocial
	$('.searchSocial').waypoint(function(direction) {
		if(direction == "up") {
			headroom.destroy();
			$('.homeHeader').removeClass('fixedHeader');
			$('.headerArea').removeClass('isFixed');
		}
	});
}

//Set Text of social media description on icon hover
var setTitle = function(title) {
	$('.currentSelection').text(title);
};

$('.lovin').hover(function() {
	setTitle("Blog Lovin'");
});

$('.facebook').hover(function() {
	setTitle('Facebook');
});

$('.pinterest').hover(function() {
	setTitle('Pinterest');
});

$('.twitter').hover(function() {
	setTitle('Twitter');
});

$('.polyvore').hover(function() {
	setTitle('Polyvore');
});

$('.searchSocial .socialIcons').on("mouseleave", function() {
	setTitle("Connect");
});

// JS for Responsive Menu

//Control Display of Contact email
	$('.contact').on("click", function(event) {
		if ($(window).width() < 780) {
			$('.emailBlock').slideToggle();
			$(this).toggleClass('hidden').toggleClass('showing');
		}
		event.preventDefault();
});

//Show/hide Menu

$(' .menuToggle').on('click', function() {
	$('.mainNav').slideToggle();
	$(this).toggleClass("icon-menu").toggleClass("icon-close");
	
});

//Init Share Button
$('.shareButton').share();

//Enquire JS Media Queries

enquire.register("screen and (max-width:780px)", {
	match: function() {
		$('.headerArea').find('.logo').remove().prependTo('.headerArea');
		$('.upperArea').find('.socialIcons').remove().appendTo('.headerArea');
		$('.sidebar').find('.search').remove().appendTo('.upperArea');
		$('.followEmail').remove().prependTo('.sidebar');
	},

	unmatch: function() {
		$('.headerArea').find('.logo').remove().appendTo('.headerArea');
		$('.headerArea').find(' > .socialIcons').remove().appendTo('.searchSocial');
		$('.upperArea').find('.search').remove().prependTo('.sidebar');
		$('.followEmail').remove().prependTo('.searchSocial');
		$('.mainNav').show();
	}
});
