<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<link rel="stylesheet" href="assets/css/global.css">
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<title>9to5Chic</title>
</head>
<body>
	<div class="container">
		<header class="siteHeader slideOutUp">
			<div class="container">
				<a href="/" class="logoSm">
					<img class="headerLogo" src="assets/img/logoSM.png" alt="9to5Chic">
				</a>

				<a href="#0" class="menuToggle icon-menu"></a>

				<div class="logoBig menuOpen">
					<a href="/"><img src="assets/img/logoLG.png" alt="9to5Chic"></a>
				</div> <!-- /.logo -->

				<div class="socialIcons">
					<a href="" class="icon-plus-sign"></a>
					<a href="" class="icon-twitter"></a>
					<a href="" class="icon-facebook"></a>
					<a href="" class="icon-pinterest"></a>
					<a href="">P</a>
				</div> <!-- /.socialIcons -->

					<nav class="mainNav" role="Navigation">
					<ul>
						<li><a href="/about.php">About</a></li>
						<li><a href="#">Wearing Lately</a></li>
						<li><a href="#">Daily Reads</a></li>
						<li class="contact hidden"><a href="#">Contact</a>
							<div class="emailBlock">
								<p><a href="#">anh@9to5chic.com</a></p>
							</div> <!-- /.emailBlock -->
						</li>
					</ul>
				</nav>

			</div> <!-- /.container -->
		</header>