<?php get_header(); ?>

		<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
		</div> <!-- /.headerAd -->

<div class="mainContent grid singlePost">
	<section class="posts col-2-3">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="blogPost">
			<p class="date"><time><?php the_time("l F j, Y"); ?></time></p>
			<h1 class="postTitle"><?php the_title(); ?></h1>
			<div class="postContent">
				<?php the_content(); ?>
				<?php include('parts/socialShare.php'); ?>
				<div class="shareButton"></div>
			</div> <!-- /.postContent -->
			<div class="categoriesLinks">Categories: <?php the_category(', '); ?></div>
		</article>
		<div class="commentsArea" id="comments">
		<?php comments_template(); ?>
	</div> <!-- /.commentsArea -->
		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>

	
	</section> <!-- /.posts -->

	<?php include("parts/sidebar.php"); ?>
</div> <!-- /.mainContent -->

<?php get_footer(); ?>
