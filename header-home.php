<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8" />
	<title><?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="google-site-verification" content="-vUKa-8G6khwRynBcIs-oiJwauzECGOKiFNISVD4bHw" />

	
	<!-- stylesheets -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/global.css"/>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300' rel='stylesheet' type='text/css'>

	<!--[if lt IE 9]>
		<script src="<?php bloginfo('template_url'); ?>/assets/js/html5shiv.js"></script>
	<![endif]-->

<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/ie.css"/>
	<![endif]-->

	<!--WP Generated Header -->
	<?php wp_head(); ?>
	<!--End WP Generated Header -->
	
</head>
<body <?php body_class($class); ?>>
		<div class="headerArea">
			
			<header class="homeHeader slideOutUp">
				<div class="container">
					<a href="/">
						<img class="headerLogo" src="<?php bloginfo('template_url'); ?>/assets/img/logoSM.png" alt="9to5Chic">
					</a>

					<a href="#0" class="menuToggle icon-menu"></a>

					<div class="socialIcons">
						<a href="http://www.bloglovin.com/blog/2122159/9to5-chic" class="icon-plus-sign lovin"></a>
						<a href="http://twitter.com/9to5chic" class="icon-twitter twitter"></a>
						<a href="https://www.facebook.com/9to5Chic" class="icon-facebook facebook"></a>
						<a href="http://www.pinterest.com/9to5chic/" class="icon-pinterest pinterest"></a>
						<a href="http://anh-9to5chic.polyvore.com/" class="polyvore">P</a>
						<span class="currentSelection">Connect</span>
					</div> <!-- /.socialIcons -->

						<nav class="mainNav" role="navigation">
						<ul>
							<li><a href="/about">About</a></li>
							<li><a href="/wearing-lately">Wearing Lately</a></li>
							<li><a href="/daily-reads">Daily Reads</a></li>
							<li class="contact hidden"><a href="#">Contact</a>
								<div class="emailBlock">
									<p><a href="mailto:anh@9to5chic.com">anh@9to5chic.com</a></p>
								</div> <!-- /.emailBlock -->
							</li>
						</ul>
					</nav>

				</div> <!-- /.container -->
			</header>

			<div class="logo">
				<a href="/">
					<img src="<?php bloginfo('template_url'); ?>/assets/img/9to5chic-logo.svg" alt="9to5Chic" onerror="this.src='<?php bloginfo('template_url'); ?>/assets/img/logoLG.png'">
				</a>
				
			</div> <!-- /.logo -->
		
		</div> <!-- /.headerArea -->
		<div class="container">