<?php get_header(); ?>

		<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
		</div> <!-- /.headerAd -->

<div class="mainContent singlePost posts">
	<section class="posts col-2-3">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="blogPost">
			<div class="postContent">
				<?php the_content(); ?>
			</div> <!-- /.postContent -->
		</article>
		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>

	</section> <!-- /.posts -->

	<?php include("parts/sidebar.php"); ?>
</div> <!-- /.mainContent -->

<?php get_footer(); ?>
