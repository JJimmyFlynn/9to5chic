$(document).ready(function() {

//Variables for Instagram Ajax Call
var dataUrl = 'https://api.instagram.com/v1/users/4507990/media/recent';
var accessParameters = 'client_id=23327ff5baf949079efc830a08cda7b3&count=10';

//Call for Latest Instagram Photos
$.ajax(dataUrl, {
	data: accessParameters,
	dataType: 'JSONP',
	//Append each image to flexslider
	success: function(data) {
		for (var i = 0; i < data.data.length; i++) {
			$('.instaSlides ul').append("<li>" + "<a href='" + data.data[i].link + "'" + " target='_blank'>" + "<img src='" +
				data.data[i].images.standard_resolution["url"] +
				"'>" + "</a>");
		}
	}
	//Initialize Instagram flexslider when Ajax and Success calls done
}).done(function() {
	$('.instaSlides').flexslider({
		controlNav: false,
		directionNav: false,
	});
});

// Initialize Main Rotator Flexslider

$('.flexslider').flexslider({
	pauseOnHover: true,
	controlNav: false,
	directionNav: true,
	slideshowSpeed: 5000,
	prevText: "",
	nextText: ""
});

//Initialize headroom.js
var elem = document.querySelector("header");
// construct an instance of Headroom, passing the element
var headroom  = new Headroom(elem, {
			"tolerance": 5,
			"classes": {
				"initial": "fixedHeader",
				"pinned": "slideInDown",
				"unpinned": "slideOutUp"
  }
});

if ($(window).width() > 780) {
		//fix header @ .mainContent to prevent flash of header
	$('.headerAd').waypoint(function(direction) {
			if(direction == "down") {
			//$('.homeHeader').addClass('fixedHeader');
			$('.headerArea').addClass('isFixed');
		}
	});

	// initialise with waypoints @ .headerAd
	$('.headerAd').waypoint(function(direction) {
				headroom.init();
	});


	//destroy headroom when scrolling up @ .searchSocial
	$('.home .searchSocial').waypoint(function(direction) {
		if(direction == "up") {
			headroom.destroy();
			$('.homeHeader').removeClass('fixedHeader');
			$('.headerArea').removeClass('isFixed');
		}
	});
$('.logo').waypoint(function(direction) {
		if(direction == "up") {
			headroom.destroy();
			$('.siteHeader').removeClass('fixedHeader');
			$('.headerArea').removeClass('isFixed');
		}
	});

}

//Set Text of social media description on icon hover
var setTitle = function(title) {
	$('.currentSelection').text(title);
};

$('.lovin').hover(function() {
	setTitle("Bloglovin'");
});

$('.facebook').hover(function() {
	setTitle('Facebook');
});

$('.pinterest').hover(function() {
	setTitle('Pinterest');
});

$('.twitter').hover(function() {
	setTitle('Twitter');
});

$('.polyvore').hover(function() {
	setTitle('Polyvore');
});

$('.searchSocial .socialIcons').on("mouseleave", function() {
	setTitle("Connect");
});

// JS for Responsive Menu

//Control Display of Contact email
	$('.contact').on("click", function(event) {
		if ($(window).width() < 780) {
			$('.emailBlock').slideToggle();
			$(this).toggleClass('hidden').toggleClass('showing');
		}
		event.preventDefault();
});

//Show/hide Menu

$('.menuToggle').on('click', function() {
	$('.mainNav').slideToggle();
	$(this).toggleClass("icon-menu").toggleClass("icon-close");
});

//Enquire JS Media Queries

enquire.register("screen and (max-width:780px)", {
	match: function() {
		$('.headerArea').find('.logo').remove().prependTo('.headerArea');
		$('.upperArea').find('.socialIcons').hide();
		$('.followEmail').remove().prependTo('.sidebar');
		$('.mainNav').hide();
	},

	unmatch: function() {
		$('.headerArea').find('.logo').remove().appendTo('.headerArea');
		$('.headerArea').find(' > .socialIcons').remove().appendTo('.searchSocial');
		$('.followEmail').remove().prependTo('.searchSocial');
		$('.mainNav').show();
		$('.upperArea').find('.socialIcons').show();
	}
});
});