<?php get_header('home'); ?>

	<section class="upperArea">
		<div class="rotator flexslider">
			<ul class="slides">
				<?php $query = new WP_Query(array(
				'post_type' => 'homepage_slides',
				'posts_per_page' => 3,
				'orderby' => 'menu_order',
				'order' => 'ASC'
			)); ?>
		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				
				<li class="slideItem">
					<img src="<?php the_field('slide_image'); ?>" alt="">
					<div class="overlay">
						<div class="overlayContent">
							<p class="category"><?php the_field('slide_category'); ?></p>
							<h2><?php the_title(); ?></h2>
							<?php the_content(); ?>
							<a href="<?php the_field('link_post'); ?>" class="styledSubmit">Read More</a>
						</div> <!-- /.overlayContent -->
					</div> <!-- /.overlay -->
				</li>

				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>
			</ul>
		</div> <!-- /.rotator -->

		<section class="searchSocial">
			<div class="followEmail col-1-3">
				<form action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=9to5chic/QrHO', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
					<input class="styledInput" type="email" name="email" placeholder="Follow Via Email"/>
					<input type="hidden" value="9to5chic/QrHO" name="uri"/>
					<input type="hidden" name="loc" value="en_US"/>
					<input class="styledSubmit" type="submit" value="Submit" />
				</form>
			</div> <!-- /.followEmail -->
			
			<div class="socialIcons">
				<a href="http://www.bloglovin.com/blog/2122159/9to5-chic" class="icon-plus-sign lovin"></a>
				<a href="http://twitter.com/9to5chic" class="icon-twitter twitter"></a>
				<a href="https://www.facebook.com/9to5Chic" class="icon-facebook facebook"></a>
				<a href="http://www.pinterest.com/9to5chic/" class="icon-pinterest pinterest"></a>
				<a href="http://anh-9to5chic.polyvore.com/" class="polyvore">P</a>
				<span class="currentSelection">Connect</span>
			</div> <!-- /.socialIcons -->

			</section>

			<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
			</div> <!-- /.headerAd -->
	</section>

	<div class="mainContent grid">

		<section class="posts col-2-3">
				<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
				<?php query_posts('showposts=3&paged=' . $paged); ?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article class="blogPost">
						<p class="date"><time><?php the_time("l F j, Y"); ?></time></p>
						<h1 class="postTitle h2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						<div class="postContent">
							<?php the_content(); ?>
							<?php include('parts/socialShare.php'); ?>
							<div class="commentsInfo"><?php comments_number();?> / <a href="<?php comments_link(); ?>">Leave a Comment</a></div>
							<div class="categoriesLinks">Categories: <?php the_category(', '); ?></div>
						</div> <!-- /.postContent -->
					</article>

				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>

				<div class="pagination">
					<div class="nav-previous"><?php next_posts_link( 'Older posts' ); ?></div>
					<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
				</div> <!-- /.pagination -->

		</section> <!-- /.posts -->

		<?php include("parts/sidebar.php"); ?>
	</div> <!-- /.mainContent -->

<?php get_footer(); ?>
