<?php
/*
Template Name: Wearing Lately
*/
?>

<?php get_header(); ?>

		<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
		</div> <!-- /.headerAd -->

		<div class="mainContent grid">

		<section class="posts col-2-3">
				<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
				<?php query_posts('category_name=wearing-now&showposts=3&paged=' . $paged); ?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<article class="blogPost">
						<p class="date"><time data-time="look into this"><?php the_time("l F j, Y"); ?></time></p>
						<h1 class="postTitle h2"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						<div class="postContent">
							<?php the_content(); ?>
							<?php include('parts/socialShare.php'); ?>
							<div class="commentsInfo"><?php comments_number();?> / <a href="<?php comments_link(); ?>">Leave a Comment</a></div>
							<div class="shareButton"></div>
							<div class="categoriesLinks">Categories: <?php the_category(', '); ?></div>
						</div> <!-- /.postContent -->
					</article>

				<?php endwhile; endif; ?>
				

				<div class="pagination">
					<div class="nav-previous"><?php next_posts_link( 'Older posts' ); ?></div>
					<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
				</div> <!-- /.pagination -->

		</section> <!-- /.posts -->

		<?php include("parts/sidebar.php"); ?>
	</div> <!-- /.mainContent -->

<?php get_footer(); ?>