<?php
/*
Template Name: About
*/
?>

<?php get_header(); ?>

		<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
		</div> <!-- /.headerAd -->
		
	<div class="mainContent aboutContain posts">
		<section class="aboutBlog">
			<h1><img src="<?php bloginfo('template_url'); ?>/assets/img/about.png" alt="About the Blog" class="aboutHeader"></h1>
			<div class="aboutImg">
				<img src="<?php the_field('biography_image'); ?>" alt="">
			</div> <!-- /.aboutImg -->

			<div class="aboutText">
				<?php the_post(); ?>
				<?php the_content(); ?>
			</div> <!-- /.aboutText -->
		</section>

		<section class="press">
			<img src="<?php bloginfo('template_url'); ?>/assets/img/press.png" alt="Press and Features" class="aboutHeader">
			<div class="linkGrid">
				<?php $query = new WP_Query(array(
				'post_type' => 'about_affiliate',
				'show_posts' => -1,
				'orderby' => 'menu_order',
				'order' => 'ASC'
			)); ?>
		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
				<div class="linkModule <?php if (!get_field('link_image')) { echo 'text'; } ?>">
					<a href="<?php the_field('link_address'); ?>">
						<?php if (get_field('link_image')) { ?>
							<img src="<?php the_field('link_image'); ?>" alt="">
						<?php } ?>
						<? if (!get_field('link_image')) { ?>
						<?php echo get_field('link_title'); ?>
						<?php } ?>
					</a>
				</div>
				<?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>
			</div>
		</section>
	</div> <!-- /.mainContent -->
<?php get_footer(); ?>