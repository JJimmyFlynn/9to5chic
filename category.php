<?php get_header(); ?>

		<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
		</div> <!-- /.headerAd -->

<div class="mainContent">
	<section class="posts col-2-3 searchPage">
	<h1>Showing Results for: <?php single_cat_title(); ?></h1>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="searchResult">
		<h2 class="postTitle"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<p class="date"><time><?php the_time("l F j, Y"); ?></time></p>
		<div class="searchExcerpt">
			<?php the_excerpt(); ?>
		</div>
	</div>
	<?php endwhile; endif; ?>
	<?php wp_reset_query(); ?>
</section>

</div> <!-- /.mainContent -->

<?php get_footer(); ?>