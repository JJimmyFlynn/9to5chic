<div class="socialShare">
	<p>Share: </p>
	<a class="icon-facebook" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink();?>" target="_blank"></a>
	<a class="icon-twitter" href="http://twitter.com/intent/tweet?text=<?php the_title();?>+<?php the_permalink();?>" target="_blank"></a>
	<a class="icon-google" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"></a>
	<a class="icon-pinterest" href="javascript:void((function(){var%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)})());" target="_blank"></a>
</div>