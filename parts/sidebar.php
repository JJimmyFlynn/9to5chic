		<aside class="sidebar col-1-3">

			<?php get_search_form(); ?>

			<div class="instagram">
				<div class="instaSlides">
					<ul class="slides">
						
					</ul>
				</div> <!-- /.instaSlides -->
			</div> <!-- /.instagram -->

			<div class="pickWeek">
				<?php $query = new WP_Query(array(
				'post_type' => 'weekly_craving',
				'posts_per_page' => 1
			)); ?>
		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

			<a href="<?php the_field('affiliate_link'); ?>" target="_blank">
				<img src="<?php the_field(craving_image); ?>" alt="">
			</a>
			
		  <?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>
			</div> <!-- /.pickWeek -->

			<!-- AOL Ad -->
			<div class="aolAd">
			<script type='text/javascript'>
				var atwMN='93373282', atwWidth='300', atwHeight='250'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
			</div><!-- End AOL AD -->

			<?php $query = new WP_Query(array(
				'post_type' => 'sidebar_ads',
				'show_posts' => -1,
				'orderby' => 'menu_order',
				'order' => 'ASC'
			)); ?>
		  <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
			<div class="userAd">
				<a href="<?php the_field('ad_link'); ?>" target="_blank">
					<img src="<?php the_field(ad_image); ?>" alt="">
				</a>
			</div>
		  <?php endwhile; endif; ?>
				<?php wp_reset_query(); ?>
		</aside>