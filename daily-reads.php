<?php
/*
Template Name: Daily Reads
*/
?>

<?php get_header(); ?>

		<div class="headerAd">
			<script type='text/javascript'>
				var atwMN='93373289', atwWidth='728', atwHeight='90'
			</script>
			<script type='text/javascript' src="http://o.aolcdn.com/ads/adsWrapper3.js"></script>
		</div> <!-- /.headerAd -->

		<div class="mainContent grid">

			<section class="posts">

				<h1><img src="<?php bloginfo('template_url'); ?>/assets/img/dailyreads.png" alt="About the Blog" class="aboutHeader"></h1>
			
				<div class="linksGrid">

				<div class="letterGroup">

		<?php 

		$query = new WP_Query(array(
			'post_type' => 'daily_reads',
			'meta_key' => 'column_grouping',
			'meta_value' => 'Column 1',
			'orderby' => 'title',
			'order' => 'ASC'
		)); ?>

		<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

		<a href="<?php the_field('read_link'); ?>" class="dailyLink"><?php the_title(); ?></a>

		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>

			</div> <!-- /.letterGroup -->

			<div class="letterGroup">

			<?php 

		$query = new WP_Query(array(
			'post_type' => 'daily_reads',
			'meta_key' => 'column_grouping',
			'meta_value' => 'Column 2',
			'orderby' => 'title',
			'order' => 'ASC'
		)); ?>

		<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

		<a href="<?php the_field('read_link'); ?>" class="dailyLink"><?php the_title(); ?></a>

		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>

			</div> <!-- /.letterGroup -->

			<div class="letterGroup">

			<?php 

		$query = new WP_Query(array(
			'post_type' => 'daily_reads',
			'meta_key' => 'column_grouping',
			'meta_value' => 'Column 3',
			'orderby' => 'title',
			'order' => 'ASC'
		)); ?>

		<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

		<a href="<?php the_field('read_link'); ?>" class="dailyLink"><?php the_title(); ?></a>

		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>

			</div> <!-- /.letterGroup -->

			<div class="letterGroup">

			<?php 

		$query = new WP_Query(array(
			'post_type' => 'daily_reads',
			'meta_key' => 'column_grouping',
			'meta_value' => 'Column 4',
			'orderby' => 'title',
			'order' => 'ASC'
		)); ?>

		<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

		<a href="<?php the_field('read_link'); ?>" class="dailyLink"><?php the_title(); ?></a>

		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>

			</div> <!-- /.letterGroup -->

		</div> <!-- /.linksGrid -->



		</section> <!-- /.posts -->

	</div> <!-- /.mainContent -->

<?php get_footer(); ?>